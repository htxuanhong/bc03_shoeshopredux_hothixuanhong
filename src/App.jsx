import "antd/dist/antd.css";
import "./App.css";
import BaiTapShoeShopRedux from "./BaiTapShoeShopRedux/BaiTapShoeShopRedux";

function App() {
  return (
    <div className="App">
      <BaiTapShoeShopRedux />
    </div>
  );
}

export default App;
