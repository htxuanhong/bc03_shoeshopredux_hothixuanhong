import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";

class Productlist extends Component {
  render() {
    return (
      <div className="container py-3">
        <div className="row">
          {/* optional */}
          {this.props.productList?.map((item, index) => {
            // console.log("dữ liệu", item);
            return <ProductItem data={item} key={index} />;
          })}{" "}
        </div>{" "}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productList: state.shoeShopReducer.productList,
  };
};

export default connect(mapStateToProps)(Productlist);
