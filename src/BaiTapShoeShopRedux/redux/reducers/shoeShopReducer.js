import { dataShoeShop } from "../../dataShoeShope";
import {
  ADD_TO_CARD,
  TANG_GIAM_SO_LUONG,
  XOA_SAN_PHAM,
} from "../constants/constant";

let initialState = {
  productList: dataShoeShop,
  gioHang: [],
};

export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CARD: {
      console.log("yes", payload);
      // xử lý logic ( đem body function trong compoenent ShoeShop)
      let cloneGioiHang = [...state.gioHang];
      let index = cloneGioiHang.findIndex((item) => {
        return item.id === payload.id;
      });
      if (index === -1) {
        let newSanPham = { ...payload, soLuong: 1 };
        cloneGioiHang.push(newSanPham);
      } else {
        cloneGioiHang[index].soLuong++;
      }
      state.gioHang = cloneGioiHang;
      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);
      return { ...state };
    }

    case XOA_SAN_PHAM:
      {
        // console.log("yes", payload);

        let cloneGioiHang = [...state.gioHang];

        let index = cloneGioiHang.findIndex((item) => {
          return item.id === payload;
        });

        // console.log({ index });
        if (index !== -1) {
          cloneGioiHang.splice(index, 1);

          state.gioHang = cloneGioiHang;

          return { ...state };
          // this.setState({ gioHang: cloneGioiHang });
        }
      }
      break;
    case TANG_GIAM_SO_LUONG: {
      let cloneGioiHang = [...state.gioHang];
      let index = cloneGioiHang.findIndex((item) => {
        return item.id === payload.idSanPham;
      });
      if (index !== -1) {
        cloneGioiHang[index].soLuong += payload.giaTri;
      }

      state.gioHang = cloneGioiHang;
      return { ...state };
    }

    default:
      return state;
  }
};
