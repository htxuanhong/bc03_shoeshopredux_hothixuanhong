import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CARD } from "./redux/constants/constant";

// {
//     "id": 7,
//     "name": "Adidas Ultraboost 4",
//     "alias": "adidas-ultraboost-4",
//     "price": 450,
//     "description": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
//     "shortDescription": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
//     "quantity": 854,
//     "image": "http://svcy3.myclass.vn/images/adidas-ultraboost-4.png"
// }
class ProductItem extends Component {
  render() {
    // console.log(this.props.data.id);

    let { name, price, image } = this.props.data;

    return (
      <div className="card col-3  ">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
        </div>
        <div className="mb-4">
          <button
            onClick={() => {
              this.props.handleThemSanPham(this.props.data);
            }}
            className="btn btn-info"
          >
            Add to card
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleThemSanPham: (sanPham) => {
      dispatch({
        type: ADD_TO_CARD,
        payload: sanPham,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(ProductItem);
