import { Popconfirm } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { TANG_GIAM_SO_LUONG, XOA_SAN_PHAM } from "./redux/constants/constant";

class Cart extends Component {
  render() {
    // console.log("gioHang", this.props?.gioHang);
    return (
      <div className="container">
        <button className="btn btn-outline-danger mb-4">
          Giỏ hàng: {this.props.gioHang.length}
        </button>

        {!!this.props.gioHang.length && (
          <table className="table">
            <thead>
              <td>ID</td>
              <td>Tên sản phảm</td>
              <td>Giá sản phẩm</td>
              <td>Số lượng</td>
              <td>Thao tác</td>
            </thead>
            <tbody>
              {this.props.gioHang.map((item) => {
                return (
                  <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSoLuong(item.id, 1);
                        }}
                        className="btn btn-success"
                      >
                        Tăng
                      </button>
                      <span className="mx-2">{item?.soLuong}</span>
                      {item.soLuong <= 1 ? (
                        <Popconfirm
                          title="Bạn muốn xóa chứ？"
                          okText="Đồng ý"
                          cancelText="Hủy"
                          onConfirm={() => {
                            this.props.handleXoaSanPham(item.id);
                          }}
                        >
                          <button className="btn btn-secondary">Giảm</button>
                        </Popconfirm>
                      ) : (
                        <button
                          onClick={() => {
                            this.props.handleTangGiamSoLuong(item.id, -1);
                          }}
                          className="btn btn-secondary"
                        >
                          Giảm
                        </button>
                      )}
                    </td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleXoaSanPham(item.id);
                        }}
                        className="btn btn-danger"
                      >
                        Xoá
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleXoaSanPham: (idSanPham) => {
      dispatch({
        type: XOA_SAN_PHAM,
        payload: idSanPham,
      });
    },
    handleTangGiamSoLuong: (idSanPham, giaTri) => {
      dispatch({
        type: TANG_GIAM_SO_LUONG,
        payload: { idSanPham, giaTri },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
