import React, { Component } from "react";
import Cart from "./Cart";
import Productlist from "./Productlist";

export default class BaiTapShoeShopRedux extends Component {
  state = {
    gioHang: [],
  };
  render() {
    return (
      <div>
        <h4 className="text-danger pt-3">Shoe Shop</h4>
        <Productlist />

        <Cart />
      </div>
    );
  }
}
// state management

// npm i redux react-redux
